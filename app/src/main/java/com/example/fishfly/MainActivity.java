package com.example.fishfly;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
{
// creating the class
    private flyingfish gameview;
    private Handler handler = new Handler();
    private final static long intervel = 30;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

//    remove this and replace setContentView(R.layout.activity_main);
        gameview = new flyingfish(this);
        setContentView(gameview);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
gameview.invalidate();
                    }
                });
            }
        },0,intervel);

    }
}
