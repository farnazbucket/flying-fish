package com.example.fishfly;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;


//1 create the View
public class flyingfish extends View {

//    Bitmap fish[] = new Bitmap;

    private Bitmap fish[] = new Bitmap[2];
    //   7. move the fish from X and Y
    private int fishX = 10;
    private int fishY;
    private int fishspeed;

    private int canvasWidth, canvasHight;

    private Bitmap backgroundimage;

    private Paint scorepaint = new Paint();

    private Bitmap life[] = new Bitmap[2];

    private boolean touch = false;


    public flyingfish(Context context) {
        super(context);
// 3.getting the resource of fish image
        fish[0] = BitmapFactory.decodeResource(getResources(), R.drawable.fish1);
//6. creating second fish
        fish[1] = BitmapFactory.decodeResource(getResources(), R.drawable.fish2);
//4. creating the background and adding the private class in the mainactivity

        backgroundimage = BitmapFactory.decodeResource(getResources(), R.drawable.background);

//       painting score  before this create the class above


        scorepaint.setColor(Color.WHITE);
        scorepaint.setTextSize(70);
        scorepaint.setTypeface(Typeface.DEFAULT);
        scorepaint.setAntiAlias(true);


        life[0] = BitmapFactory.decodeResource(getResources(), R.drawable.hearts);
        life[1] = BitmapFactory.decodeResource(getResources(), R.drawable.heart_grey);

        fishY = 550;

    }

// 2.we need onDraw for canvas

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);

        canvasWidth = canvas.getWidth();
        canvasHight = canvas.getHeight();


// 5 draw background and the life
        canvas.drawBitmap(backgroundimage, 0, 0, null);

        int minFishY = fish[0].getHeight();
        int maxFishY = canvasHight - fish[0].getHeight() * 3;
        fishY = fishY + fishspeed;
        if (fishY < minFishY) {
            fishY = minFishY;
        }
        if (fishY > minFishY) {
            fishY = maxFishY;




//            after this use the touch
        }

        fishspeed = fishspeed+2;

        if (touch) {

        canvas.drawBitmap(fish [1],fishX,fishY,null);

        touch = false;
        }

        else
        {
            canvas.drawBitmap(fish[0],fishX,fishY,null);
        }

// drawing the fish, next create the flyingfish view call in main activity

//        again here fish have to fly so make it fly by writing the canvas hight and width
//        canvas.drawBitmap(fish,0,0,null);

        canvas.drawText("Score", 20, 20, scorepaint);
        canvas.drawBitmap(life[0], 580, 10, null);
        canvas.drawBitmap(life[0], 680, 10, null);
        canvas.drawBitmap(life[0], 780, 10, null);

//         next we move the fish by creating the private class of fish


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) ;
        {
            touch = true;

            fishspeed = -22;

        }

        return true;

    }
}


